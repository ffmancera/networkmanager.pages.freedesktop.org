+++
date = "2020-06-17T12:00:00+01:00"
title = "Internet connection sharing with NetworkManager [link]"
tags = ["connection sharing"]
draft = false
author = "Beniamino Galvani"
description = "Share your Internet connectivity with other devices"
weight = 10
+++

NetworkManager is the network configuration daemon used on Fedora and
many other distributions. It provides a consistent way to configure
network interfaces and other network-related aspects on a Linux
machine. Among many other features, it provides a Internet connection
sharing functionality that can be very useful in different situations.

Read more on the [Fedora Magazine](https://fedoramagazine.org/internet-connection-sharing-networkmanager/).
