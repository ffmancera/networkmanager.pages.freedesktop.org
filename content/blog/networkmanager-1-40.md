+++
date = "2022-08-25T15:00:00+02:00"
title = "NetworkManager 1.40"
tags = ["release"]
draft = false
author = "Thomas Haller"
description = "What's new"
weight = 10
+++

This is the announcement for the new 1.40.0 NetworkManager release.
The tarball can be found on our [download page](https://download.gnome.org/sources/NetworkManager/1.40/)
page.

This release comes a bit more than 3 months after 1.38.0 release and contains about 600 patches
from 29 contributors.

See the [NEWS](https://gitlab.freedesktop.org/NetworkManager/NetworkManager/-/blob/1.40.0/NEWS) file
for a list of noteworthy changes and some highlights below.

![Flecky welcomes NetworkManager](/blog/images/flecky.jpg "Flecky welcomes NetworkManager")


## `nmcli` Offline Commands

`nmcli` now supports an `--offline` flag, which can process connection profiles
in keyfile format without requiring the NetworkManager daemon.

Using NetworkManager is all about connection profiles, which represent a networking
configuration of one interface. Alongside other UIs, the `nmcli connection` subcommand
allows to show, create, delete and modify profiles. Until now, those operations
were always talking to the NetworkManager daemon via the D-Bus API.

On disk, profiles get persisted in NetworkManager's keyfile format as documented in
[the nm-settings-keyfile manual](/docs/api/latest/nm-settings-keyfile.html).
The user directly can modify those files under
`/{usr/lib,etc,run}/NetworkManager/system-connections`.

`nmcli` now got a flag to directly output and consume keyfile format.

For example, `nmcli --offline connection add ...` takes the same arguments as the regular `add` command,
but instead of telling NetworkManager to add the profile it prints the profile in keyfile format to stdout.

Likewise, `cat "$FILE" | nmcli --offline connection modify ...`
will read the profile from stdin, modify it, and print the result to stdout.

This allows nmcli to be used as command line tool for generating and editing keyfile profiles.

```
# NetworkManager will require the file to be unaccessible to non-root users,
# that is, `chmod 600`
umask 077

# Generate a profile in keyfile format.
nmcli --offline connection add type ethernet con-name my-profile \
  | tee /etc/NetworkManager/system-connections/my-profile.nmconnection

# Modify a profile in keyfile format.
nmcli --offline connection modify connection.mptcp-flags enabled,signal \
   < /etc/NetworkManager/system-connections/my-profile.nmconnection \
   | tee /etc/NetworkManager/system-connections/my-profile.nmconnection~

mv /etc/NetworkManager/system-connections/my-profile.nmconnection~ \
   /etc/NetworkManager/system-connections/my-profile.nmconnection

# After modifying files on disk, we need to tell NetworkManager about
# it.
nmcli connection reload
```

## MPTCP Support

[Multipath TCP](https://en.wikipedia.org/wiki/Multipath_TCP)
(MPTCP) is an extension of TCP. Good introductions can be found
on Davide Caratti's ([\*](https://developers.redhat.com/blog/2020/08/19/multipath-tcp-on-red-hat-enterprise-linux-8-3-from-0-to-1-subflows#)),
Andrea Claudi's ([\*](https://developers.redhat.com/articles/2021/10/20/multipath-tcp-rhel-8-one-many-subflows#))
or Christian Horn's ([\*](https://www.redhat.com/en/blog/using-multipath-tcp-better-survive-outages-and-increase-bandwidth))
blogs.

NetworkManager can now do endpoint management for MPTCP. Endpoints are IP addresses that can
be announced or used for additional subflows. Those can be configured manually with iproute2's
`ip mptcp endpoint` command or automatically by the `mptcpd` daemon. NetworkManager now also automatically
configures endpoints, similar to `mptcpd`'s address notification feature.

MPTCP can be enabled/disabled in kernel via the `/proc/sys/net/mptcp/enabled` sysctl.
There are also limits in kernel that control how subflows get created. See iproute2's `ip mptcp limits`
command. NetworkManager will neither enabled/disable the MPTCP sysctl or adjust those limits. These
settings are considered host configuration, which the administrator is supposed to set up
or the distribution ships with sensible defaults.

Each NetworkManager profile has now a new property `connection.mptcp-flags` which controls
MPTCP handling. In particular, this flags property allows to enable/disable MPTCP handling or
select the address flags that are used when configuring endpoint (see `signal`, `subflow`, `backup`,
or `fullmesh` in `man ip-mptcp`). If MPTCP handling for an active profile is enabled, NetworkManager will
configure the IP addresses as endpoints, using the specified address flags.

Most interesting is that the user doesn't actually need to set `connection.mptcp-flags`.
By leaving the flags at the default, MPTCP handling will be automatically used based on
whether MPTCP is enabled the `/proc/sys/net/mptcp/enabled` sysctl.

If you run the new NetworkManager, you can see the configured endpoints with `ip mptcp endpoint`
command.

Note that NetworkManager now performs a similar task as `mptcpd` does. If you have `mptcpd` running,
you may want to either disable `mptcpd` or NetworkManager's MPTCP handling. You can do that by either
setting the `connection.mptcp-flags` per profile, or by configuring the global connection defaults in a file
`/etc/NetworkManager/conf.d/95-no-mptcp.conf` that contains

```
[connection.no-mptcp]
# Configure a global connection default for the per-profile setting
# "connection.mptcp-flags". 0x1 means "disabled".
connection.mptcp-flags=0x1
```

Also be aware that a strict `rp_filter` breaks MPTCP use-cases. So if MPTCP handling on an interface
is enabled, NetworkManager will relax a strict setting ("1") to loose reverse path filtering ("2").

See [the nm-settings manual](/docs/api/latest/nm-settings-nmcli.html#nm-settings-nmcli.property.connection.mptcp-flags) for details.


## Read DHCP lease from file

When NetworkManager does DHCP, the user can get the lease information via
the D-Bus API. For example, `nmcli -f all device show eth0` will show that
information.

Now this information is also written to a state file, and can be fetched from there.

The files `/run/NetworkManager/devices/$IFINDEX` contain now `[dhcp4]` and
`[dhcp6]` sections, where `$IFINDEX` is the index of the interface as kernel
knows it. You can find the ifindex via `ip link` command.

## IPv4 Link Local Addresses together with Manual/DHCP Configuration

There is a new profile setting `ipv4.link-local` to enable IPv4 link local addresses.
Previously, IPv4LL could only be configured via `ipv4.method=link-local`. Now,
link local can be configured in addition to manual addresses or auto/DHCP.


## Acknowledgements

Many thanks to all contributors who provided feedback, ideas or patches.

Adrian Freihofer, Alex Henrie, Ana Cabral, Andrew Zaborowski, Antonio
Alvarez Feijoo, Beniamino Galvani, Christian Glombek, David Bauer, David
Rheinsberg, Dominique Martinet, Dylan Van Assche, Fernando Fernandez
Mancera, Georg Müller, Igor Ponomarev, Lubomir Rintel, Martin Blanchard,
NorwayFun, Olivier Gayot, Ryosuke YASUOKA, Slava Monich, Thomas Haller,
Vojtech Bubela, Wen Liang, Yi Zhao, Yuri Chornoivan, avery, bluikko,
liaohanqin, 谢致邦 (XIE Zhibang)

Also thanks to our Quality Engineers from Red Hat for all the testing.

Vladimír Beneš, Filip Pokryvka, David Jasa and Matej Berezny.

Join us on our [GitLab project](https://gitlab.freedesktop.org/NetworkManager/NetworkManager/).
