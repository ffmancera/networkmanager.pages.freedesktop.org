+++
date = "2024-02-26T08:00:00+00:00"
title = "NetworkManager 1.46"
tags = ["release"]
draft = false
author = "Fernando F. Mancera"
description = "What's new"
weight = 10
+++

A little bit more than half a year and 481 commits since NetworkManager 1.44, a new release
is ready: NetworkManager 1.46.

Let's take a look at the most interesting parts!

## Drop build with python2, python3 is now required

Building with python2 is not possible anymore. In order to build
NetworkManager, python3 is now a requirement. Considering python2 is EOL since
January 1, 2020, we considered it was time to drop it aswell.

## Support randomizing the MAC address based on the Wi-Fi network

NetworkManager is introducing `wifi.cloned-mac-address=stable-ssid` to allow
randomizing the MAC address based on the Wi-Fi network's SSID. The goal behind
this change is to improve user privacy as there are some network operators and
advertisers tracking MAC addresses to collect user information about their
movements and device usage patterns

The change was also
[accepted](https://fedoraproject.org/wiki/Changes/StableSSIDMACAddress) in
Fedora Linux 40 as the default behavior.

## IPv4 DAD (Duplicate Address Detection) enabled by default

A duplicate address is a serious issues which leads to non-working setups or
problems hard to debug. We decided to enable DAD by default to detect such
problems.

For more details see also the
[accepted](https://fedoraproject.org/wiki/Changes/Enable_IPv4_Address_Conflict_Detection)
change in Fedora Linux 40.

## What else?

Now the NetworkManager VPN plugins have support for two-factor authentication
(2FA).

SR-IOV support to configure the NIC's eswitch settings via Devlink using the
properties `sriov.eswitch-mode`, `sriov.eswitch-inline-mode` and
`sriov.eswitch-encap-mode`.

It is now possible to create interface types not directly supported by
NetworkManager by defining a generic connection with the generic.device-handler
property set. The property specifies the name of a dispatcher script to be
called to create and destroy the interface. NetworkManager will then take care
of the IP configuration on that interface.

Plus a lot of bugfixes, new features and translation updates. See
[NEWS](https://gitlab.freedesktop.org/NetworkManager/NetworkManager/-/blob/1.46.0/NEWS)
file for even more detailed summary of what's new.

## Acknowledgements

Many thanks to all contributors who provided feedback, ideas or patches.

Aleksandr Melman, Beniamino Galvani, Emmanuel Grumbach, Fernando Fernandez
Mancera, Frederic Martinsons, Gris Ge, Íñigo Huguet, Jan Vaclav, Javier Sánchez
Parra, Johannes Zink, Korbin Bickel, Luna Jernberg, Michael Biebl, Mohammed
Sadiq, Sergey Koshelenko, Sicelo A. Mhlongo, Stanislas Faye, Tatsuyuki Ishi,
Thomas Haller, Wen Liang, Yuki Inoguchi, Yuri Chornoivan.

Also thanks to our Quality Engineers from Red Hat for all the testing:
Vladimír Beneš, Filip Pokryvka, David Jasa and Matej Berezny.

Join us on our [GitLab project](https://gitlab.freedesktop.org/NetworkManager/NetworkManager/).

## Get the new release

As usual, the next release of your favorite Linux distribution will surely
ship the new version.

In case you're too impatient to wait, or you are, in fact, responsible
for keeping NetworkManager up to date in a distribution, get the tarball
from our [download page](https://download.gnome.org/sources/NetworkManager/1.46/).

Thanks for tuning in and goodbye!
