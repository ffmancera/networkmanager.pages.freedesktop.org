+++
date = "2022-12-5:00:00+01:00"
title = "Set up NMCI on Fedora Linux VM"
tags = ["NMCI"]
draft = false
author = "Vojtech Bubela"
description = "Learn how to run NetworkManager CI tests on Fedora Linux VM"
weight = 10
+++

In this article I will show you how to set up NetworkManager CI tests on Fedora Linux virtual machine.

## What is NMCI

NMCI stands for NetworkManager countinuous integration. It is project on gitlab that
provides tests for NetworkManager. The test are written in python behave enviroment.
Every test is written in a way acceptance criteria would be written. You can find out
more on the NMCI project [gitlab page](https://gitlab.freedesktop.org/NetworkManager/NetworkManager-ci).

## Why VM?

Sometimes it can be useful to have quick access to NetworkManager's CI tests. Maybe
when you want to edit a test or run a smaller scale test multiple times. It is not
recommended to run NMCI on your own system because it does system-wise changes (adds
users, install packages, enable services, etc. ).

## Set up Fedora Linux VM

You can use any VM software. Make sure that your VM will be able to connect to the
internet. You can check that by typing `nmcli` in the terminal. You should see atleast one
device with active connection. If there is none you will need to create one. After that
clone [NMCI gitlab repository](https://gitlab.freedesktop.org/NetworkManager/NetworkManager-ci).

## Set up NetworkManager CI

From the root folder of the NMCI project run `test_run.sh` script.
This script needs to be run with a test name from `mapper.yaml` as a argument to pass.
When run, for example like this
```
test_run.sh bond_config_file
``` 
for the first time after clonning the repo it will install any missing dependecies NMCI
needs.

## Running the tests

Every test belongs to one of the features (bond, bridge, ipv6, etc.). To see all tests
of a feature go to `features/scenarios/*.feature`. To run all tests a feature has can
be one by running 
```
runfeature.sh <feature-name>
```
script in `run/` directory.
	There is also an option to run only part of these tests. That can be done by running 
```
runtest.sh <test-name>
```
in `run/` directory. Name of different test can be either found directly in
`/feature/scenario/*.feature` file or in `mapper.yaml` file.

## Things to look out for

Sometimes a device will not automatically connect when restarting VM or when
running `test_run.sh` for first time. If that happens you will need to manually
connect it by using 
```
nmcli device connect <name-of-device>
```

