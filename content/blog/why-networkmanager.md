+++
date = "2020-04-10T12:00:00+01:00"
title = "Why NetworkManager? [link]"
categories = [""]
draft = false
author = "Thomas Haller"
description = "NetworkManager is more than just a tool to configure the network"
weight = 10
+++

What makes NetworkManager unique among network configuration projects?

Read more on [Thomas Haller's Blog](https://blogs.gnome.org/thaller/2020/04/10/why-networkmanager/).
