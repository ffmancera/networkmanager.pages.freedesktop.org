+++
draft= false
title = "Community"
description = "How to get in touch with the NetworkManager community."
+++

### Repository and issue tracker

https://gitlab.freedesktop.org/NetworkManager/NetworkManager

### Mailing list

[networkmanager@lists.freedesktop.org](mailto:networkmanager@lists.freedesktop.org)
([list](https://lists.freedesktop.org/mailman/listinfo/networkmanager),
[archive](https://lists.freedesktop.org/archives/networkmanager/),
[old archive](https://mail.gnome.org/archives/networkmanager-list/))

### IRC

#nm on [Libera.Chat](https://libera.chat/)

### Fediverse/Mastodon

[@NetworkManager@fosstodon.org](https://fosstodon.org/@NetworkManager)

### Monthly Online Community Meetups

See [meetups](/community/meetup).

### Links

- [discourse.gnome.org](https://discourse.gnome.org/search?q=NetworkManager%20order%3Alatest) [[category](https://discourse.gnome.org/tag/networkmanager)]
- stackexchange
  - [askubuntu.com](https://askubuntu.com/questions/tagged/network-manager)
  - [serverfault.com](https://serverfault.com/questions/tagged/networkmanager)
  - [stackoverflow.com](https://stackoverflow.com/questions/tagged/networkmanager)
  - [unix.stackexchange.com](https://unix.stackexchange.com/questions/tagged/networkmanager)
- [discussion.fedoraproject.org](https://discussion.fedoraproject.org/search?q=NetworkManager%20order%3Alatest)
- [reddit.com](https://www.reddit.com/search/?q=NetworkManager&t=week)
