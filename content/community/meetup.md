+++
draft= false
title = "Community Meetup"
description = "Our monthly NetworkManager community open for everybody."
+++

### Community Meetup

Interested to meet? Want to talk about something? Have a suggestion,
feedback, questions? Join our monthly online meeting.

Everybody is welcome. The topics are those that you bring!

We meet **online every 2nd Wednesday of the month** (unless it's a public holiday),
at 12:00 pm, UTC.

 - [meet.google.com](https://meet.google.com/qcj-pbfa-ucr)

If you wish to join, you can also first reach out on networkmanager@lists.freedesktop.org or our IRC channel
(#nm on Libera.Chat).

The meetup is also announced on our [mailing list](https://lists.freedesktop.org/archives/networkmanager/).
