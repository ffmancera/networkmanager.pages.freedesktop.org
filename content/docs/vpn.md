---
title: "VPN support"
date:
weight: 7
summary: ""
description: ""
bref: ""
---

NetworkManager supports VPN connections for all popular VPN connections via plugins.

A VPN plugin consists of the editor dialog and a D-Bus service that
manages the actual VPN connection.

### VPN plugins maintained in GNOME

This is a list of VPN plugins that are maintained as GNOME projects:

|                                                        |                                          |
| ------------------------------------------------------ | ---------------------------------------- |
| [NetworkManager-fortisslvpn](https://gitlab.gnome.org/GNOME/NetworkManager-fortisslvpn) ([releases](https://download.gnome.org/sources/NetworkManager-fortisslvpn)) | Fortinet SSLVPN compatible |
| [NetworkManager-libreswan](https://gitlab.gnome.org/GNOME/NetworkManager-libreswan) ([releases](https://download.gnome.org/sources/NetworkManager-libreswan)) | IPsec IKEv1 VPN, Cisco compatible |
| [NetworkManager-openconnect](https://gitlab.gnome.org/GNOME/NetworkManager-openconnect) ([releases](https://download.gnome.org/sources/NetworkManager-openconnect/)) | Cisco AnyConnect, Juniper |
| [NetworkManager-openvpn](https://gitlab.gnome.org/GNOME/NetworkManager-openvpn) ([releases](https://download.gnome.org/sources/NetworkManager-openvpn)) | OpenVPN |
| [NetworkManager-pptp](https://gitlab.gnome.org/GNOME/NetworkManager-pptp) ([releases](https://download.gnome.org/sources/NetworkManager-pptp)) | PPTP, Microsoft compatible |
| [NetworkManager-sstp](https://gitlab.gnome.org/GNOME/network-manager-sstp) | SSTP compatible VPN plugin |
| [NetworkManager-vpnc](https://gitlab.gnome.org/GNOME/NetworkManager-vpnc) ([releases](https://download.gnome.org/sources/NetworkManager-vpnc)) | IPsec VPN, Cisco compatible |

### Natively Supported by NetworkManager

|                                                        |                                          |
| ------------------------------------------------------ | ---------------------------------------- |
| [WireGuard](https://www.wireguard.com/) | NetworkManager 1.16.0+ [supports WireGuard natively](https://blogs.gnome.org/thaller/2019/03/15/wireguard-in-networkmanager/) and requires no plugin |

### VPN plugins maintained by third parties

There are also plugins which are not maintained by NetworkManager
developers. Visit their web sites to check them out:

|                                                       |                                           |
| ----------------------------------------------------- | ----------------------------------------- |
| [NetworkManager-anyconnect](https://github.com/grahamwhiteuk/Networkmanager-anyconnect) | Fortinet SSLVPN compatible |
| [NetworkManager-iodine](https://honk.sigxcpu.org/piki/projects/network-manager-iodine/) | Tunnel IP traffic via DNS using Iodine |
| [NetworkManager-l2tp](https://github.com/nm-l2tp/network-manager-l2tp) | L2TP compatible VPN plugin |
| [NetworkManager-ssh](https://github.com/danfruehauf/NetworkManager-ssh) | Connect using OpenSSH's Tunnel capability |
| [NetworkManager-strongswan](https://wiki.strongswan.org/projects/strongswan/wiki/NetworkManager) | IKEv2 enables IPsec plugin with support for EAP, PSK and certificate authentication |

### Plugin compatibility note

NetworkManager maintains backward compatibility with older plugin
versions. That means that the plugin version 0.9.10 will work with
later NetworkManager versions, such as 1.2.

For VPN plugins, the major-minor version indicates the minimal
required NetworkManager version. It is therefore expected and correct
that for certain NetworkManager versions there exists no matching VPN
plugin version. For example, for NetworkManager-pptp might not exist a
1.4.0 version because the latest 1.2.x version is suitable and
up-to-date to run against latest NetworkManager.
