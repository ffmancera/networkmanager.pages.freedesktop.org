#!/bin/bash

# In the past, our documentation was hosted at https://developer.gnome.org/NetworkManager/
# and https://developer.gnome.org/libnm/. But that tooling is no longer supported and
# recent documentation is no longer there.
#
# Instead, it's at our website, like
# https://networkmanager.dev/docs/api/latest/NetworkManager.html and
# https://networkmanager.dev/docs/api/1.34.0/NetworkManager.html
#
# Run this script to import a new documentation. It downloads the release
# tarball from https://download.gnome.org/sources/NetworkManager/, which
# contains pre-generated docs. It then copies them to the versioned location.
#
# Pass "-l" if (and only if) the newly documentation is the latest version.
# That causes the docs to also be copied (and available) via a "latest" link,
# like https://networkmanager.dev/docs/api/latest/.
#
# By default, the script will also update the major alias, for example, importing
# https://networkmanager.dev/docs/api/1.42.2 will also create https://networkmanager.dev/docs/api/1.42.
# That is for importing the latest minor release of a stable branch. You can
# suppress that with "-L".
#
# Example:
#   $ ./scripts/import-docs.sh 1.34.0 -l
#   $ #... review
#   $ git push

set -e

die() {
    printf '%s\n' "$*" >&2
    exit 1
}

usage() {
    echo "$0 <VERSION> [ -l | -L ]"
    echo
    echo " By default, the script updates the major links like https://networkmanager.dev/docs/api/1.40/."
    echo " That can be suppressed by passing '-L'"
    echo " If '-l' is specified, the script also updates the latest links like https://networkmanager.dev/docs/api/latest."
    exit 0
}

ARGS=("$0" "$@")

CMD="${ARGS[*]}"

unset VERSION
DO_LATEST_MAJ=0
DO_LATEST_MIN=1
for a ; do
    case "$a" in
        -h)
            usage
            ;;
        -L)
            DO_LATEST_MAJ=0
            DO_LATEST_MIN=0
            ;;
        -l)
            DO_LATEST_MAJ=1
            DO_LATEST_MIN=1
            ;;
        *)
            test -z "${VERSION+x}" || die "Invalid argument \"$a\""
            VERSION="$1"
            ;;
    esac
done
test -n "${VERSION+x}" || usage

OLD_IFS="$IFS"
IFS=.
VERSION_ARRAY=( $VERSION )
IFS="$OLD_IFS"
VERSION_MIN=

case "${#VERSION_ARRAY[@]}" in
    2)
        test "$VERSION" == "${VERSION_ARRAY[0]}.${VERSION_ARRAY[1]}" || die "invalid version $VERSION"
        DO_LATEST_MIN=0
        ;;
    3)
        test "$VERSION" == "${VERSION_ARRAY[0]}.${VERSION_ARRAY[1]}.${VERSION_ARRAY[2]}" || die "invalid version $VERSION"
        VERSION_MIN="${VERSION_ARRAY[0]}.${VERSION_ARRAY[1]}"
        ;;
    4)
        test "$VERSION" == "${VERSION_ARRAY[0]}.${VERSION_ARRAY[1]}.${VERSION_ARRAY[2]}.${VERSION_ARRAY[3]}" || die "invalid version $VERSION"
        DO_LATEST_MIN=0
        ;;
    *)
        die "invalid version $VERSION"
        ;;
esac

FILENAME_BASE="NetworkManager-$VERSION"

cd "$(git rev-parse --show-toplevel)"

detect_filename_from_csum_ext() {
    local SHA="$1"
    local F
    local EXT="$2"

    F="$(sed -n 's/^[a-f0-9]\+[    ]\+\(NetworkManager-[0-9].*tar'"$EXT"'\)$/\1/p' "$SHA" | sed '1!d')"
    test -n "$F" || return 1
    printf '%s' "$F"
}

detect_filename_from_csum() {
    local SHA="$1"
    local EXT
    local F

    for EXT in '\.xz' '\.bz2' '\.[a-z0-9]*' ; do
        F="$(detect_filename_from_csum_ext "$SHA" "$EXT")"
        if [ $? == 0 ]; then
            printf '%s' "$F"
            return 0
        fi
    done
    return 1
}

download_tarball() {
    local URL
    local csum

    FILENAME_TAR=
    for csum in sha256sum md5sum; do
        URL="https://download.gnome.org/sources/NetworkManager/${VERSION_ARRAY[0]}.${VERSION_ARRAY[1]}/$FILENAME_BASE.$csum"
        if wget -O "./tmp/$FILENAME_BASE.$csum" "$URL" ; then
            FILENAME_TAR="$(detect_filename_from_csum "./tmp/$FILENAME_BASE.$csum")"
            break
        fi
    done

    test -n "$FILENAME_TAR" || die "failed to detect filename by searching for sha256sum/md5sum file at https://download.gnome.org/sources/NetworkManager/${VERSION_ARRAY[0]}.${VERSION_ARRAY[1]}/"

    URL="https://download.gnome.org/sources/NetworkManager/${VERSION_ARRAY[0]}.${VERSION_ARRAY[1]}/$FILENAME_TAR"
    wget -O "./tmp/$FILENAME_TAR" "$URL" || die "failed to download tarball at $URL"

    sed "/$FILENAME_TAR$/!d" -i "./tmp/$FILENAME_BASE.$csum"
    grep -F "$FILENAME_TAR" "./tmp/$FILENAME_BASE.$csum" || die "invalid sha file does not mention $FILENAME_TAR"
    test -f "./tmp/$FILENAME_TAR" || die "file ./tmp/$FILENAME_TAR not found"
    (cd ./tmp && $csum -c "./$FILENAME_BASE.$csum") || die "invalid file $FILENAME_TAR"
}

extract_tarball() {
    rm -rf "./tmp/$FILENAME_BASE/" 2>/dev/null
    (cd ./tmp && tar -xvf "./$FILENAME_TAR") || die "failure to extract ./tmp/$FILENAME_TAR"
    DIRNAME="$FILENAME_BASE"
    if ! test -d "./tmp/$DIRNAME/"; then
        DIRNAME="$(printf '%s' "$DIRNAME" | sed 's/-rc[0-9]\+$//')"
        test -d "./tmp/$DIRNAME/" || die "extracted archive not found"
    fi
}

detect_docs() {
    local D
    local has=0

    for D in $(ls -1d "./tmp/$DIRNAME/docs"/*/html/ 2>/dev/null) ; do
        D="${D%/html/}"
        D="${D##*/}"
        printf '%s\n' "$D"
        has=1
    done
    if [ "$has" = 0 ] ; then
        printf '%s\n' "No docs found in ./tmp/$DIRNAME/docs/*/html/" >&2
        return 1
    fi
}

move_docs() {
    local D

    for D in "${DOC_DIRS[@]}" ; do
        mkdir -p "./content/docs/$D/"
        rm -rf "./content/docs/$D/$VERSION/"
        mv "./tmp/$DIRNAME/docs/$D/html/" "./content/docs/$D/$VERSION/"
        if [ "$DO_LATEST_MAJ" = 1 ] ; then
            rm -rf "./content/docs/$D/latest"
            cp -r "./content/docs/$D/$VERSION/" "./content/docs/$D/latest"
        fi
        if [ "$DO_LATEST_MIN" = 1 ] ; then
            rm -rf "./content/docs/$D/$VERSION_MIN"
            cp -r "./content/docs/$D/$VERSION/" "./content/docs/$D/$VERSION_MIN"
        fi
    done
}

commit() {
    local D

    for D in "${DOC_DIRS[@]}" ; do
        git add -f "./content/docs/$D/$VERSION/"
        if [ "$DO_LATEST_MAJ" = 1 ] ; then
            git add -f "./content/docs/$D/latest/"
        fi
        if [ "$DO_LATEST_MIN" = 1 ] ; then
            git add -f "./content/docs/$D/$VERSION_MIN/"
        fi
    done
    git commit -m "docs: import documentation from $FILENAME_TAR

sha256sum: $(sha256sum "./tmp/$FILENAME_TAR" | sed 's/^\([a-f0-9]\+\)[	 ]\+.*/\1/')

  $ $CMD"
}

mkdir -p ./tmp/

FILENAME_TAR=
download_tarball
DIRNAME=
extract_tarball
DOC_DIRS=( $(detect_docs) ) || exit 0
move_docs
commit

rm -rf "./tmp/$FILENAME_TAR"
rm -rf "./tmp/$FILENAME_BASE/"
